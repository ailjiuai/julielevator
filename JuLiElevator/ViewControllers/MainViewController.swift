//
//  MainViewController.swift
//  JuLiElevator
//
//  Created by iLogiEMAC on 16/3/1.
//  Copyright © 2016年 JuLi. All rights reserved.
//

import UIKit
@IBDesignable
class MainViewController: BaseViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.redColor()
    }
}
