//
//  LeftMenuButton.swift
//  JuLiElevator
//
//  Created by zp on 16/2/28.
//  Copyright © 2016年 JuLi. All rights reserved.
//

import UIKit

class LeftMenuButton: UIButton {

   override init(frame: CGRect) {
        super.init(frame: frame)
        setConfig()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
                setConfig()
    }
    
    func setConfig() {
        self.imageView?.contentMode = UIViewContentMode.Center
        self.titleLabel?.textAlignment = NSTextAlignment.Center
       self.contentHorizontalAlignment = .Center
        self.contentVerticalAlignment = .Center
    }

}
