//
//  HomeViewController.swift
//  JuLiElevator
//
//  Created by iLogiEMAC on 16/3/1.
//  Copyright © 2016年 JuLi. All rights reserved.
//

import UIKit
@IBDesignable
class HomeViewController: BaseViewController {

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view
        //setup导航
        buildNavigationUI()
    }
    func buildNavigationUI() {
        
        let imageView = UIImageView(frame: CGRectMake(0, 0, 100, 40))
        imageView.image = UIImage(named: "Joylive LOGO-hdpi")
        self.navigationItem.leftBarButtonItem =  UIBarButtonItem(customView: imageView)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
