//
//  ViewController.swift
//  JuLiElevator
//
//  Created by 张振东 on 16/2/23.
//  Copyright (c) 2016年 JuLi. All rights reserved.
//

import UIKit
class ViewController: UIViewController {


    @IBOutlet weak var loginName: UITextField!
    @IBOutlet weak var loginPassword: UITextField!
    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var loginBtn: UIButton!
    
   
    var hud : MBProgressHUD =  MBProgressHUD(frame: UIScreen.mainScreen().bounds)
    var contentVC = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle()).instantiateViewControllerWithIdentifier("BaseNav") as? UINavigationController
    
//    var leftMenuVC: UIViewController = UIStoryboard(name: "Home", bundle: NSBundle.mainBundle()).instantiateViewControllerWithIdentifier("LeftMenuVC")
    
    
        override func viewDidLoad() {
        super.viewDidLoad()

        //初始化 hud
        initHud();
        //设置上面的登录按钮样式
        loginBtn.backgroundColor = UIColor.redColor()
        loginBtn.layer.cornerRadius = 7;
       
        
    }
    
    //初始化 hud
    func initHud() {
         hud.center = self.view.center
         self.view.addSubview(hud)
    }
    //MARK: 记住密码
    @IBAction func remerberPassword(sender: UIButton) {
        
        sender.selected =  !sender.selected
    }
    //MARK: 监听开始编辑
    @IBAction func beginEdit(sender: UITextField) {
        
        logoImage.image = UIImage(named: "Joylive LOGO-hdpi");
        logoImage.contentMode = UIViewContentMode.Bottom
    }
    //MARK: 监听结束编辑
    @IBAction func endEdit(sender: AnyObject) {
        logoImage.image = UIImage(named: "LogoPic-hdpi");
        logoImage.contentMode = UIViewContentMode.ScaleAspectFill
    }
    @IBAction func loginButton(sender: UIButton) {
        
        hud.show(true)
        hud.bringSubviewToFront(self.view)
        
        let name = loginName.text
        let password = loginPassword.text
        if (name != nil) && (password != nil) {
            let dic = ["number":name!,"password":password!]
            //MARK: －  登录请求
            HTTPClient.shareHTTPClient().requestHTTPURLWithPath("/api/maintence/ios/login", parameters: dic, method: GET, success: { (responesObject, error) -> Void in
                self.hud.hide(true)
               
                let success = responesObject.objectForKey("success") as! Bool
                if(success) {
                    MBProgressHUD.showSuccess("登陆成功", toView: self.view.window)
                    let json = responesObject["data"]!!["info"];
                    //MARK: MJExtension
                    let user = UserInfo.mj_objectWithKeyValues(json!)
                    //存储用户的数据
                    NSUserDefaults.standardUserDefaults().setObject(user.number, forKey: user_number)
                    NSUserDefaults.standardUserDefaults().setObject(user.ID, forKey: user_id)
                    NSUserDefaults.standardUserDefaults().setObject(user.telephone, forKey: user_telephone)
                    NSUserDefaults.standardUserDefaults().setObject(user.name, forKey: user_name)
                    NSUserDefaults.standardUserDefaults().setObject(user.maintainerId, forKey: user_maintainerId)
                    
                    //FIXME: 此处ResideMenu代理有问题,稍后修改
//                    let resideMenu = RESideMenu(contentViewController: self.contentVC, leftMenuViewController:self.leftMenuVC, rightMenuViewController: nil);
//                    resideMenu.contentViewScaleValue = 1.0
//                    resideMenu.scaleMenuView = false
//                    resideMenu.scaleContentView = false
//                    resideMenu.bouncesHorizontally = false
//                    resideMenu.fadeMenuView = false
//                    resideMenu.contentViewInPortraitOffsetCenterX = 90 - (self.view?.bounds.size.width)! * 0.5
                    
                    
                    self.view.window?.rootViewController = self.contentVC
                }else  {
                    let errMessage = responesObject.objectForKey("description") as! String
                    MBProgressHUD.showError(errMessage, toView: self.view.window)
                }
                }) { (data, error) -> Void in
                    print("error--\(error)")
            }
        }
        
       
    }
       
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}


