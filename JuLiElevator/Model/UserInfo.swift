//
//  UserInfo.swift
//  JuLiElevator
//
//  Created by zp on 16/2/28.
//  Copyright © 2016年 JuLi. All rights reserved.
//

import Foundation


class  UserInfo : NSObject {
    var name: String?
    var number: String?
    var telephone: String?
    var maintainerId: String?
    var ID: String?
    
    override class func mj_replacedKeyFromPropertyName() -> [NSObject : AnyObject]! {
        return ["ID":"id"]
    }
    
}