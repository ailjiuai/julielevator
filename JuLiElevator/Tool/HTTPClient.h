//
//  HTTPClient.h
//  JuLiElevator
//
//  Created by zp on 16/2/28.
//  Copyright © 2016年 JuLi. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum : NSUInteger {
    GET = 1,
    POST,
    DELETE,
} HTTPMethod;
@interface HTTPClient : NSObject
+ (HTTPClient *)shareHTTPClient;

- (void)requestHTTPURLWithPath:(NSString *)path parameters:(id)parameters method:(HTTPMethod)method success:(void (^)(id data,NSError * error))success failure:(void(^)(id data,NSError * error))failure;
@end
