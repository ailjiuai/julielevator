//
//  LeftMenuController.swift
//  JuLiElevator
//
//  Created by zp on 16/2/27.
//  Copyright © 2016年 JuLi. All rights reserved.
//

import UIKit

class LeftMenuController: UIViewController {

    @IBOutlet weak var commissionBtn: LeftMenuButton!
    var tempSelectedBtn : LeftMenuButton?
  
   
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
       
        handleClick(commissionBtn)
    }
  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func handleClick(sender: LeftMenuButton) {
        tempSelectedBtn?.backgroundColor = UIColor.lightGrayColor()
        tempSelectedBtn?.selected = false
        sender.selected = !sender.selected
        tempSelectedBtn = sender
        var titleStr: String?
        var identifier: String?
        switch sender.tag {
        case 1:
           
            sender.backgroundColor = sender.selected ? UIColor(red: 224/255.0, green: 40/255.0, blue: 55/255.0, alpha: 1.0) : UIColor.lightGrayColor()
            titleStr = "我的代办";
            identifier = "MainNav"
        case 2:
            sender.backgroundColor = sender.selected ? UIColor(red: 179/255.0, green: 135/255.0, blue: 11/255.0, alpha: 1.0) : UIColor.lightGrayColor()
            titleStr = "我的项目";
             identifier = "ProjectNav"
        case 3:

            sender.backgroundColor = sender.selected ? UIColor(red: 47/255.0, green: 159/255.0, blue: 193/255.0, alpha: 1.0) : UIColor.lightGrayColor()
             titleStr = "活动报备";
             identifier = "ActivityNav"
        case 4:

            sender.backgroundColor = sender.selected ? UIColor(red: 54/255.0, green: 177/255.0, blue: 156/255.0, alpha: 1.0) : UIColor.lightGrayColor()
             titleStr = "报价单";
            identifier = "PriceNav"
        case 5:
            sender.backgroundColor = sender.selected ? UIColor.grayColor() : UIColor.lightGrayColor()
            titleStr = ""
            identifier = "MoreNav"
        default: break
            
        }
        let contentVC = UIStoryboard(name: "Home", bundle: NSBundle.mainBundle()).instantiateViewControllerWithIdentifier(identifier!) as? UINavigationController
         (contentVC?.visibleViewController  as! BaseViewController).navTitle = titleStr
        (contentVC?.visibleViewController  as! BaseViewController).titleColor = sender.backgroundColor
        contentVC?.navigationBar.tintColor =  sender.backgroundColor
        self.sideMenuViewController.setContentViewController(contentVC, animated: true)
        self.sideMenuViewController.hideMenuViewController()
    }
    
   

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
