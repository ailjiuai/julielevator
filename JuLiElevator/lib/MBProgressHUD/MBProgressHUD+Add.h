//
//  MBProgressHUD+Add.h
//  视频客户端
//
//  Created by mj on 13-4-18.
//  Copyright (c) 2013年 itcast. All rights reserved.
//

#import "MBProgressHUD.h"

@interface MBProgressHUD (Add)
+ (void)showError:(NSString *)error toView:(UIView *)view;
+ (void)showSuccess:(NSString *)success toView:(UIView *)view;

+ (MBProgressHUD *)showMessag:(NSString *)message toView:(UIView *)view;

// 显示提示信息
+(void)showHint:(NSString *)title toView:(UIView *)view;
+(void)showHint:(NSString *)title toView:(UIView *)view decreaseOffSetY:(CGFloat )decreaseOffSetY;
@end
