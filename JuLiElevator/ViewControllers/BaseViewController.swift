//
//  MainViewController.swift
//  JuLiElevator
//
//  Created by zp on 16/2/28.
//  Copyright © 2016年 JuLi. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

 
    
    var user: UserInfo!
    //懒加载
    lazy  var titleLabel: UILabel = {
        let   label  = UILabel(frame: CGRectMake(0, 0, self.view.frame.size.width, 40))
        label.textAlignment = NSTextAlignment.Center
        label.backgroundColor = UIColor.clearColor()
        label.textColor = UIColor.whiteColor()
        label.font = UIFont.systemFontOfSize(17)
         self.navigationItem.titleView = label
        return label
    }()

    var navTitle: String? {
        willSet {
            titleLabel.text = newValue
        }
    }
    var titleColor: UIColor? {
        willSet {
            titleLabel.textColor = newValue
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        UIApplication.sharedApplication().statusBarStyle = .LightContent
        
    }
    
    
    @IBAction func clickLeftNavigationItem(sender: UIBarButtonItem) {
        self.sideMenuViewController.presentLeftMenuViewController()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

 

}
