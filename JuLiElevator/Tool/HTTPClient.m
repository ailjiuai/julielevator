//
//  HTTPClient.m
//  JuLiElevator
//
//  Created by zp on 16/2/28.
//  Copyright © 2016年 JuLi. All rights reserved.
//

#import "HTTPClient.h"
#import "AFNetworking.h"

#define kBaseURL @"http://58.210.173.54:50020"
@implementation HTTPClient
{
    AFHTTPSessionManager * manager;
    
}
+ (HTTPClient *)shareHTTPClient
{
    static HTTPClient * client = nil;
    if (client == nil) {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            client = [[self alloc]init];
        });
    }
    return  client;
}
- (id)init
{
    self = [super init];
    if (self) {
        NSURLSessionConfiguration * config = [NSURLSessionConfiguration defaultSessionConfiguration];
        manager = [[AFHTTPSessionManager alloc]initWithBaseURL:[NSURL URLWithString:kBaseURL] sessionConfiguration:config];
        
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        
        
    }
    return self;
}
- (void)requestHTTPURLWithPath:(NSString *)path parameters:(id)parameters method:(HTTPMethod)method success:(void (^)(id  _Nonnull data,NSError * _Nullable error))success failure:(void(^)(id _Nullable data,NSError * _Nonnull error))failure
{
    path = [path stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    switch (method) {
        case GET:
        {
            [manager GET:path parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                success(responseObject,nil);
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                failure(nil,error);
            }];
        }
            break;
        case POST:
        {
            [manager POST:path parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                success(responseObject,nil);
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                failure(nil,error);
            }];
        }
            break;
        default:
            break;
    }
}
@end
